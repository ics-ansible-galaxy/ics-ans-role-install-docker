# ics-ans-role-install-docker

**DEPRECATED: Please use ics-ans-role-docker**

Ansible role to install install-docker.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml

# Edition can be one of: 'ce' (Community Edition) or 'ee' (Enterprise Edition).
docker_edition: 'ce'
docker_package: "docker-{{ docker_edition }}"
docker_package_state: present

# Service options.
docker_service_state: started
docker_service_enabled: true
docker_restart_handler_state: restarted

docker_yum_repo_url: https://download.docker.com/linux/{{ (ansible_distribution == "Fedora") | ternary("fedora","centos") }}/docker-{{ docker_edition }}.repo
docker_yum_repo_enable_edge: 0
docker_yum_repo_enable_test: 0
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-install-docker
```

## License

BSD 2-clause
