import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker_service(host):
    docker = host.service("docker.service")
    assert docker.is_enabled
    assert docker.is_running


def test_docker_listens(host):
    docker = host.socket("unix:///var/run/docker.sock")
    assert docker.is_listening
